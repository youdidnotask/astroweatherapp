package com.pam.astroweatherapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.pam.astroweatherapp.database.DBManager;
import com.pam.astroweatherapp.database.AstroReaderContract.*;
import com.pam.astroweatherapp.service.NetworkService;
import com.pam.astroweatherapp.service.YahooWeatherService;

public class LocationsActivity extends AppCompatActivity {
    private DBManager dbManager;
    private SimpleCursorAdapter adapter;

    private String[] locationsData = new String[] {CityEntry._ID, CityEntry.COLUMN_TITLE, CityEntry.COLUMN_LAT, CityEntry.COLUMN_LNG};
    private int[] locationsView = new int[] {R.id.cityId, R.id.cityTitle, R.id.cityLat, R.id.cityLng};

    private ListView listView;

    private void init() {
        dbManager = new DBManager(this);
        dbManager.open();
        Cursor cursor = dbManager.fetch();
        listView = (ListView) findViewById(R.id.locationsListView);
        adapter = new SimpleCursorAdapter(this, R.layout.fragment_city, cursor, locationsData, locationsView, 0);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);

        init();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Long selected_id = getId(view);
                returnLocationInfo(selected_id);
                Toast.makeText(getApplicationContext(), "Click! "+selected_id, Toast.LENGTH_SHORT).show();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                long selected_id = getId(view);
                deleteLocation(selected_id);
                Toast.makeText(getApplicationContext(), "LongClick! "+selected_id, Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbManager.close();
    }

    private long getId(View selectedView) {
        return Long.parseLong(((TextView)selectedView.findViewById(R.id.cityId)).getText().toString());
    }

    private void returnLocationInfo(long id) {
        City city = dbManager.get(id);

        Intent intent = new Intent();
        intent.putExtra(MainActivity.WOEID_KEY, city.woeid);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void deleteLocation(long id) {
        dbManager.delete(id);
        adapter.changeCursor(dbManager.fetch());
        adapter.notifyDataSetChanged();
    }

    public void addLocation(View view) {
        String newLocation = ((TextView)findViewById(R.id.locationNew)).getText().toString();
        if(newLocation != null && !newLocation.equals("") && verifyConnection()) {
            setLoadingOn();
            new LocationInsert().execute(newLocation);
        }
    }

    private boolean verifyConnection() {
        boolean connected = NetworkService.verifyConnection(this);
        if(!connected) {
            Toast.makeText(getApplicationContext(), "no connection", Toast.LENGTH_SHORT).show();
        }
        return connected;
    }

    private class LocationInsert extends AsyncTask<String, Void, City> {

        @Override
        protected City doInBackground(String... locations) {
            return YahooWeatherService.getLocationInfo(locations[0]);
        }

        @Override
        protected void onPostExecute(City city) {
            if(city != null) {
                dbManager.insert(city.title, city.woeid, city.lat, city.lng);
                setLoadingOff();
                adapter.changeCursor(dbManager.fetch());
                adapter.notifyDataSetChanged();
                clear();
            }
            else {
                Toast.makeText(getApplicationContext(), "Location not found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void clear() {
        ((TextView)findViewById(R.id.locationNew)).setText("");
    }

    private void setLoadingOn() {
        ((ProgressBar)findViewById(R.id.loadingProgressBar)).setVisibility(View.VISIBLE);
    }

    private void setLoadingOff() {
        ((ProgressBar)findViewById(R.id.loadingProgressBar)).setVisibility(View.INVISIBLE);
    }
}
