package com.pam.astroweatherapp;

import android.app.Activity;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {
    private static final int STEP = 5;
    private static final String UNIT = "min";

    private Switch speedSwitch;
    private Switch tempSwitch;
    private SeekBar freqSeekBar;
    private TextView freqText;

    private void init() {
        speedSwitch = (Switch) findViewById(R.id.speedUnitsSwitch);
        tempSwitch = (Switch) findViewById(R.id.tempUnitsSwitch);
        freqSeekBar = (SeekBar) findViewById(R.id.settingsFrequencySeekBar);
        freqText = (TextView) findViewById(R.id.settingsFrequencyValue);
    }

    private void set() {
        Intent intent = getIntent();

        freqSeekBar.setOnSeekBarChangeListener(new BetterListener());

        speedSwitch.setChecked(intent.getBooleanExtra(MainActivity.SPEED_KEY, MainActivity.UNIT_DEFAULT));
        tempSwitch.setChecked(intent.getBooleanExtra(MainActivity.TEMP_KEY, MainActivity.UNIT_DEFAULT));
        freqSeekBar.setProgress(frequencyToProgress(intent.getIntExtra(MainActivity.FREQ_KEY, MainActivity.FREQUENCY_DEFAULT)));
        freqText.setText(String.format("%d " + UNIT, intent.getIntExtra(MainActivity.FREQ_KEY, MainActivity.FREQUENCY_DEFAULT)));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        init();
        set();
    }

    public static int frequencyToProgress(int freq) {
        return freq/STEP - 1;
    }
    public static int progressToFrequency(int progress) {
        return (progress+1)*STEP;
    }

    private class BetterListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            freqText.setText(String.format("%d " + UNIT, progressToFrequency(progress)));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(MainActivity.SPEED_KEY, speedSwitch.isChecked());
        intent.putExtra(MainActivity.TEMP_KEY, tempSwitch.isChecked());
        intent.putExtra(MainActivity.FREQ_KEY, progressToFrequency(Integer.valueOf(freqSeekBar.getProgress())));
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
