package com.pam.astroweatherapp.data;

import org.json.JSONObject;

import java.io.Serializable;

public class Wind implements Generation, Serializable {
    public double direction;
    public double speed;

    @Override
    public void generate(JSONObject data) {
        direction = data.optDouble("direction");
        speed = data.optDouble("speed");
    }
}
