package com.pam.astroweatherapp.data;

import org.json.JSONObject;

public interface Generation {
    public void generate(JSONObject data);
}
