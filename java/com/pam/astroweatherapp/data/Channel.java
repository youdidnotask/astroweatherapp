package com.pam.astroweatherapp.data;

import org.json.JSONObject;

import java.io.Serializable;

public class Channel implements Generation, Serializable {
    public Location location;
    public Wind wind;
    public Atmosphere atmosphere;
    public Item item;
    public Long lastUpdate;

    @Override
    public void generate(JSONObject data) {
        location = new Location();
        location.generate(data.optJSONObject("location"));

        wind = new Wind();
        wind.generate(data.optJSONObject("wind"));

        atmosphere = new Atmosphere();
        atmosphere.generate(data.optJSONObject("atmosphere"));

        item = new Item();
        item.generate(data.optJSONObject("item"));
    }
}
