package com.pam.astroweatherapp.data;

import org.json.JSONObject;

import java.io.Serializable;

public class Forecast implements Generation, Serializable {
    public int code;
    public String day;
    public int high;
    public int low;
    public String text;

    @Override
    public void generate(JSONObject data) {
        code = data.optInt("code");
        day = data.optString("day");
        high = data.optInt("high");
        low = data.optInt("low");
        text = data.optString("text");
    }
}
