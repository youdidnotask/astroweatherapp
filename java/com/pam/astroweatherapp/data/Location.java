package com.pam.astroweatherapp.data;


import org.json.JSONObject;

import java.io.Serializable;

public class Location  implements Generation, Serializable {
    public String city;
    public String country;

    @Override
    public void generate(JSONObject data) {
        city = data.optString("city");
        country = data.optString("country");
    }
}
