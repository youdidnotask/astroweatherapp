package com.pam.astroweatherapp.data;

import android.util.Log;

import com.pam.astroweatherapp.service.SerializationService;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;

public class Data {
    private SerializationService service;
    private HashMap<Integer, Channel> channels;

    public Data(String filename) {
        service = new SerializationService(filename);
        channels = service.load();
        if(channels == null) {
            channels = new HashMap<>();
        }
    }

    public void add(int woeid, Channel channel) {
        if(channels.containsKey(woeid)) {
            channels.remove(woeid);
        }
        channel.lastUpdate = Calendar.getInstance().getTimeInMillis();
        channels.put(woeid, channel);
        backup();
    }

    public boolean contains(int woeid) {
        return channels.containsKey(woeid);
    }

    public Channel get(int woeid) {
        return channels.get(woeid);
    }

    private void backup() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                service.save(channels);
            }
        }).start();
    }
}
