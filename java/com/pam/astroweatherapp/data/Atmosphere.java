package com.pam.astroweatherapp.data;

import org.json.JSONObject;

import java.io.Serializable;

public class Atmosphere implements Generation, Serializable {
    public double humidity;
    public double pressure;
    public double visibility;

    @Override
    public void generate(JSONObject data) {
        humidity = data.optDouble("humidity");
        pressure = data.optDouble("pressure");
        visibility = data.optDouble("visibility");
    }
}
