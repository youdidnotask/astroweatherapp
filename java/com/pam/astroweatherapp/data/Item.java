package com.pam.astroweatherapp.data;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;

public class Item implements Generation, Serializable {
    public double lat;
    public double lng;
    public Forecast[] forecast;

    @Override
    public void generate(JSONObject data) {
        lat = data.optDouble("lat");
        lng = data.optDouble("long");

        JSONArray forecastData = data.optJSONArray("forecast");
        forecast = new Forecast[forecastData.length()];
        for(int i=0; i<forecastData.length(); ++i) {
            forecast[i] = new Forecast();
            forecast[i].generate(forecastData.optJSONObject(i));
        }
    }
}
