package com.pam.astroweatherapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pam.astroweatherapp.City;
import com.pam.astroweatherapp.database.AstroReaderContract.*;

import java.util.ArrayList;

public class DBManager {
    private SQLiteOpenHelper dbHelper;
    private SQLiteDatabase database;
    private Context context;

    private String[] columns = new String[]{CityEntry._ID, CityEntry.COLUMN_TITLE, CityEntry.COLUMN_WOEID, CityEntry.COLUMN_LAT, CityEntry.COLUMN_LNG};

    public DBManager(Context context) {
        this.context = context;
    }

    public void open() throws SQLException {
        dbHelper = new AstroReaderDbHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Cursor fetch() {
        Cursor cursor = database.query(
                CityEntry.TABLE_NAME,
                columns,
                null, null, null, null,
                CityEntry.COLUMN_TITLE + " ASC"
        );
        if(cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public boolean insert(String title, int woeid, double lat, double lng) {
        ContentValues values = new ContentValues();
        values.put(CityEntry.COLUMN_TITLE, title);
        values.put(CityEntry.COLUMN_WOEID, woeid);
        values.put(CityEntry.COLUMN_LAT, lat);
        values.put(CityEntry.COLUMN_LNG, lng);

        return (database.insert(CityEntry.TABLE_NAME, null, values) != -1);
    }

    public City get(long id) {
        long tmpId;
        String tmpTitle;
        int tmpWoeid;
        double tmpLat, tmpLng;

        Cursor cursor = database.query(
                CityEntry.TABLE_NAME,
                columns,
                CityEntry._ID + " = " + id,
                null, null, null, null);

        if(cursor.getCount() == 1) {
            cursor.moveToFirst();
            tmpId = cursor.getLong(cursor.getColumnIndexOrThrow(CityEntry._ID));
            tmpTitle = cursor.getString(cursor.getColumnIndexOrThrow(CityEntry.COLUMN_TITLE));
            tmpWoeid = cursor.getInt(cursor.getColumnIndexOrThrow(CityEntry.COLUMN_WOEID));
            tmpLat = cursor.getDouble(cursor.getColumnIndexOrThrow(CityEntry.COLUMN_LAT));
            tmpLng = cursor.getDouble(cursor.getColumnIndexOrThrow(CityEntry.COLUMN_LNG));
            return new City(tmpId, tmpTitle, tmpWoeid, tmpLat, tmpLng);
        }
        return null;
    }

    public ArrayList<City> getAll() {
        long tmpId;
        String tmpTitle;
        int tmpWoeid;
        double tmpLat, tmpLng;

        ArrayList<City> cities = new ArrayList<>();

        Cursor cursor = this.fetch();

        while(cursor.moveToNext()) {
            tmpId = cursor.getLong(cursor.getColumnIndexOrThrow(CityEntry._ID));
            tmpTitle = cursor.getString(cursor.getColumnIndexOrThrow(CityEntry.COLUMN_TITLE));
            tmpWoeid = cursor.getInt(cursor.getColumnIndexOrThrow(CityEntry.COLUMN_WOEID));
            tmpLat = cursor.getDouble(cursor.getColumnIndexOrThrow(CityEntry.COLUMN_LAT));
            tmpLng = cursor.getDouble(cursor.getColumnIndexOrThrow(CityEntry.COLUMN_LNG));
            cities.add(new City(tmpId, tmpTitle, tmpWoeid, tmpLat, tmpLng));
        }
        cursor.close();

        return cities;
    }

    public boolean delete(Long id) {
        return (database.delete(CityEntry.TABLE_NAME, CityEntry._ID + " = " + id, null) != 0);
    }
}
