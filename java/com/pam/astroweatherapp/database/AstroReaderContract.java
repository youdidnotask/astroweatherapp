package com.pam.astroweatherapp.database;

import android.provider.BaseColumns;

public class AstroReaderContract {
    private AstroReaderContract() {};

    public static class CityEntry implements BaseColumns {
        public static final String TABLE_NAME = "cities";
        public static final String COLUMN_TITLE = "name";
        public static final String COLUMN_WOEID = "woeid";
        public static final String COLUMN_LAT = "latitude";
        public static final String COLUMN_LNG = "longitude";
    }
}
