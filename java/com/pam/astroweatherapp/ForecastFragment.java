package com.pam.astroweatherapp;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pam.astroweatherapp.service.AppService;
import com.pam.astroweatherapp.data.Channel;

public class ForecastFragment extends Fragment implements MainActivity.UpdateListener {
    private final static int NUM = 7;

    private final static String
            TEMP_C_FORMAT = "%.0f\u00B0C",
            TEMP_F_FORMAT = "%.0f F";

    private final static String
        CODES_KEY = "CODES",
        DAYS_KEY = "DAYS",
        TEMPS_KEY = "TEMPS",
        TEMP_UNIT_KEY = "TEMP_UNIT";

    private int[] codes = new int[NUM];
    private String[] days = new String[NUM];
    private double[] temps = new double[NUM];
    private boolean tempUnitSI;

    public static ForecastFragment newInstance(Channel channel, boolean sUnitSI, boolean tUnitSI) {
        ForecastFragment forecastFragment = new ForecastFragment();

        if(channel != null) {
            Bundle args = new Bundle();
            for (int i = 0; i < NUM; ++i) {
                args.putInt("CODE_" + i + "_KEY", channel.item.forecast[i].code);
                args.putString("DAY_" + i + "_KEY", channel.item.forecast[i].day);
                args.putDouble("TEMP_" + i + "_KEY", channel.item.forecast[i].high);
            }
            args.putBoolean(TEMP_UNIT_KEY, tUnitSI);
            forecastFragment.setArguments(args);
        }

        return forecastFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity parentActivity = (MainActivity)context;
        parentActivity.availableFragments.put("ID_FORECAST", this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forecast, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            codes = savedInstanceState.getIntArray(CODES_KEY);
            days = savedInstanceState.getStringArray(DAYS_KEY);
            temps = savedInstanceState.getDoubleArray(TEMPS_KEY);
            tempUnitSI = savedInstanceState.getBoolean(TEMP_UNIT_KEY);
            setView();
        }
        else if(getArguments() != null) {
            for(int i=0; i<NUM; ++i) {
                codes[i] = getArguments().getInt("CODE_" + i + "_KEY");
                days[i] = getArguments().getString("DAY_" + i + "_KEY");
                temps[i] = getArguments().getDouble("TEMP_" + i + "_KEY");
            }
            tempUnitSI = getArguments().getBoolean(TEMP_UNIT_KEY);
            setView();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putIntArray(CODES_KEY, codes);
        outState.putStringArray(DAYS_KEY, days);
        outState.putDoubleArray(TEMPS_KEY, temps);
        outState.putBoolean(TEMP_UNIT_KEY, tempUnitSI);
    }

    private int getResourceId(int code) {
        return this.getResources().getIdentifier("forecast"+code, "drawable", getActivity().getPackageName());
    }

    private void setView() {
        ((TextView)getView().findViewById(R.id.forecastDay_01)).setText(days[0]);
        ((TextView)getView().findViewById(R.id.forecastDay_02)).setText(days[1]);
        ((TextView)getView().findViewById(R.id.forecastDay_03)).setText(days[2]);
        ((TextView)getView().findViewById(R.id.forecastDay_04)).setText(days[3]);
        ((TextView)getView().findViewById(R.id.forecastDay_05)).setText(days[4]);
        ((TextView)getView().findViewById(R.id.forecastDay_06)).setText(days[5]);
        ((TextView)getView().findViewById(R.id.forecastDay_07)).setText(days[6]);

        ((ImageView)getView().findViewById(R.id.forecastImageView_01)).setImageResource(getResourceId(codes[0]));
        ((ImageView)getView().findViewById(R.id.forecastImageView_02)).setImageResource(getResourceId(codes[1]));
        ((ImageView)getView().findViewById(R.id.forecastImageView_03)).setImageResource(getResourceId(codes[2]));
        ((ImageView)getView().findViewById(R.id.forecastImageView_04)).setImageResource(getResourceId(codes[3]));
        ((ImageView)getView().findViewById(R.id.forecastImageView_05)).setImageResource(getResourceId(codes[4]));
        ((ImageView)getView().findViewById(R.id.forecastImageView_06)).setImageResource(getResourceId(codes[5]));
        ((ImageView)getView().findViewById(R.id.forecastImageView_07)).setImageResource(getResourceId(codes[6]));

        if(tempUnitSI) {
            ((TextView)getView().findViewById(R.id.forecastTemp_01)).setText(String.format(TEMP_C_FORMAT, AppService.fahrenheitToCelsius(temps[0])));
            ((TextView)getView().findViewById(R.id.forecastTemp_02)).setText(String.format(TEMP_C_FORMAT, AppService.fahrenheitToCelsius(temps[1])));
            ((TextView)getView().findViewById(R.id.forecastTemp_03)).setText(String.format(TEMP_C_FORMAT, AppService.fahrenheitToCelsius(temps[2])));
            ((TextView)getView().findViewById(R.id.forecastTemp_04)).setText(String.format(TEMP_C_FORMAT, AppService.fahrenheitToCelsius(temps[3])));
            ((TextView)getView().findViewById(R.id.forecastTemp_05)).setText(String.format(TEMP_C_FORMAT, AppService.fahrenheitToCelsius(temps[4])));
            ((TextView)getView().findViewById(R.id.forecastTemp_06)).setText(String.format(TEMP_C_FORMAT, AppService.fahrenheitToCelsius(temps[5])));
            ((TextView)getView().findViewById(R.id.forecastTemp_07)).setText(String.format(TEMP_C_FORMAT, AppService.fahrenheitToCelsius(temps[6])));
        }
        else {
            ((TextView)getView().findViewById(R.id.forecastTemp_01)).setText(String.format(TEMP_F_FORMAT, temps[0]));
            ((TextView)getView().findViewById(R.id.forecastTemp_02)).setText(String.format(TEMP_F_FORMAT, temps[1]));
            ((TextView)getView().findViewById(R.id.forecastTemp_03)).setText(String.format(TEMP_F_FORMAT, temps[2]));
            ((TextView)getView().findViewById(R.id.forecastTemp_04)).setText(String.format(TEMP_F_FORMAT, temps[3]));
            ((TextView)getView().findViewById(R.id.forecastTemp_05)).setText(String.format(TEMP_F_FORMAT, temps[4]));
            ((TextView)getView().findViewById(R.id.forecastTemp_06)).setText(String.format(TEMP_F_FORMAT, temps[5]));
            ((TextView)getView().findViewById(R.id.forecastTemp_07)).setText(String.format(TEMP_F_FORMAT, temps[6]));
        }
    }

    @Override
    public void update(Channel channel, boolean speedUnitSI, boolean tempUnitSI) {
        if(channel != null) {
            for (int i = 0; i < NUM; ++i) {
                codes[i] = channel.item.forecast[i].code;
                days[i] = channel.item.forecast[i].day;
                temps[i] = channel.item.forecast[i].high;
            }
            this.tempUnitSI = tempUnitSI;
            setView();
        }
    }
}
