package com.pam.astroweatherapp;

public class City {
    public Long id;
    public String title;
    public int woeid;
    public double lat;
    public double lng;

    public City(String title, int woeid, double lat, double lng) {
        this.title = title;
        this.woeid = woeid;
        this.lat = lat;
        this.lng = lng;
    }

    public City(long id, String title, int woeid, double lat, double lng) {
        this.id = id;
        this.title = title;
        this.woeid = woeid;
        this.lat = lat;
        this.lng = lng;
    }
}
