package com.pam.astroweatherapp.service;

        import android.net.Uri;

        import com.pam.astroweatherapp.City;
        import com.pam.astroweatherapp.data.Channel;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.MalformedURLException;
        import java.net.URL;
        import java.net.URLConnection;

public class YahooWeatherService {

    static public City getLocationInfo(String location) {
        String query = String.format("select * from geo.places(1) where text=\"%s\"", location);
        String endpoint = String.format("https://query.yahooapis.com/v1/public/yql?q=%s&format=json", Uri.encode(query));

        try {
            JSONObject data = new JSONObject(getContent(endpoint));
            JSONObject queryData = data.optJSONObject("query");
            int countData = queryData.getInt("count");

            if(countData == 0) {
                return null;
            }
            else {
                JSONObject resultsData = queryData.optJSONObject("results");
                JSONObject placeData = resultsData.optJSONObject("place");
                JSONObject centroidData = placeData.optJSONObject("centroid");

                String title = placeData.getString("name");
                int woeid = placeData.getInt("woeid");
                Double latitude = centroidData.getDouble("latitude");
                Double longitude = centroidData.getDouble("longitude");
                return new City(title, woeid, latitude, longitude);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    static public Channel getWeatherInfo(int woeid) {
        String query = String.format("select * from weather.forecast where woeid=%d", woeid);
        String endpoint = String.format("https://query.yahooapis.com/v1/public/yql?q=%s&format=json", Uri.encode(query));

        try {
            Channel channel = new Channel();

            JSONObject data = new JSONObject(getContent(endpoint));
            JSONObject queryData = data.optJSONObject("query");
            int countData = queryData.getInt("count");

            if(countData == 0) {
                return null;
            }
            else {
                JSONObject resultsData = queryData.optJSONObject("results");
                JSONObject channelData = resultsData.optJSONObject("channel");
                channel.generate(channelData);
                return channel;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    static private String getContent(String endpoint) {
        InputStream inputStream = null;
        BufferedReader reader = null;
        try {
            URL url = new URL(endpoint);
            URLConnection connection = url.openConnection();
            connection.setUseCaches(false);

            inputStream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder content = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null){
                content.append(line);
            }

            return content.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(inputStream!=null) inputStream.close();
                if(reader!=null) reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
