package com.pam.astroweatherapp.service;

import android.app.Application;
import android.util.Log;

import com.pam.astroweatherapp.data.Channel;
import com.pam.astroweatherapp.data.Data;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class SerializationService {
    private final String filename;

    public SerializationService(String filename) {
        this.filename = filename;
    }

    public void save(HashMap<Integer, Channel> data) {
        FileOutputStream fileStream = null;
        ObjectOutputStream objectStream = null;
        try {
            fileStream = new FileOutputStream(filename);
            objectStream = new ObjectOutputStream(fileStream);

            objectStream.writeObject(data);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(objectStream != null) objectStream.close();
                if(fileStream != null) fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public HashMap<Integer, Channel> load() {
        FileInputStream fileStream = null;
        ObjectInputStream objectStream = null;
        try {
            fileStream = new FileInputStream(filename);
            objectStream = new ObjectInputStream(fileStream);

            return (HashMap<Integer, Channel>)objectStream.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if(objectStream != null) objectStream.close();
                if(fileStream != null) fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void saveSelected(Integer data) {
        FileOutputStream fileStream = null;
        ObjectOutputStream objectStream = null;
        try {
            fileStream = new FileOutputStream(filename);
            objectStream = new ObjectOutputStream(fileStream);

            objectStream.writeObject(data);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(objectStream != null) objectStream.close();
                if(fileStream != null) fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Integer loadSelected() {
        FileInputStream fileStream = null;
        ObjectInputStream objectStream = null;
        try {
            fileStream = new FileInputStream(filename);
            objectStream = new ObjectInputStream(fileStream);

            return (Integer)objectStream.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if(objectStream != null) objectStream.close();
                if(fileStream != null) fileStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
