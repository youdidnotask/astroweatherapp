package com.pam.astroweatherapp.service;

public abstract class AppService {

    public static double fahrenheitToCelsius(double f) {
        return Math.round((f-32.0)*(5.0/9.0));
    }

    public static double milesToKilometers(double mph) {
        return mph*1.609344;
    }
}
