package com.pam.astroweatherapp.service;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

import java.util.Calendar;
import java.util.GregorianCalendar;

public abstract class AstroCalculatorService {
    private static AstroCalculator getCurrentAstroCalculator(double lat, double lng) {
        GregorianCalendar calendar = new GregorianCalendar();
        AstroDateTime adt = new AstroDateTime(  calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH) + 1,
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND),
                calendar.getTimeZone().getRawOffset()/(60*60*1000),
                calendar.getTimeZone().useDaylightTime());
        return new AstroCalculator(adt, new AstroCalculator.Location(lat, lng));
    }

    public static AstroCalculator.SunInfo getCurrentSunInfo(double lat, double lng) {
        return getCurrentAstroCalculator(lat, lng).getSunInfo();
    }

    public static AstroCalculator.MoonInfo getCurrentMoonInfo(double lat, double lng) {
        return  getCurrentAstroCalculator(lat, lng).getMoonInfo();
    }
}
