package com.pam.astroweatherapp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.astrocalculator.AstroCalculator;
import java.util.Locale;
import com.pam.astroweatherapp.MainActivity.UpdateListener;
import com.pam.astroweatherapp.data.Channel;
import com.pam.astroweatherapp.service.AstroCalculatorService;

public class SunFragment extends Fragment implements UpdateListener {
    private final static String
            TIME_FORMAT = "%02d:%02d",
            COORD_FORMAT = "%.2f\u00B0";

    private final static String
            LAT_KEY = "latitude",
            LNG_KEY = "longitude";

    private final static String
            SUNRISE_TIME_KEY = "SUNRISE_TIME",
            SUNRISE_AZIMUTH_KEY = "SUNRISE_AZIMUTH",
            SUNSET_TIME_KEY = "SUNSET_TIME",
            SUNSET_AZIMUTH_KEY = "SUNSET_AZIMUTH",
            TW_MRN_TIME_KEY = "TW_MRN_TIME",
            TW_EV_TIME_KEY = "TW_EV_TIME";

    private String
            sunriseTime, sunriseAzimuth,
            sunsetTime, sunsetAzimuth,
            twilightMorningTime, twilightEveningTime;

    public static SunFragment newInstance(Channel channel, boolean speedUnitSI, boolean tempUnitSI) {
        SunFragment sunFragment = new SunFragment();

        if(channel != null) {
            Bundle args = new Bundle();
            args.putDouble(LAT_KEY, channel.item.lat);
            args.putDouble(LNG_KEY, channel.item.lng);
            sunFragment.setArguments(args);
        }

        return sunFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity parentActivity = (MainActivity)context;
        parentActivity.availableFragments.put("ID_SUN", this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sun, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            sunriseTime = savedInstanceState.getString(SUNRISE_TIME_KEY);
            sunriseAzimuth = savedInstanceState.getString(SUNRISE_AZIMUTH_KEY);
            sunsetTime = savedInstanceState.getString(SUNSET_TIME_KEY);
            sunsetAzimuth = savedInstanceState.getString(SUNSET_AZIMUTH_KEY);
            twilightMorningTime = savedInstanceState.getString(TW_MRN_TIME_KEY);
            twilightEveningTime = savedInstanceState.getString(TW_EV_TIME_KEY);
            setView();
        }
        else {
            if(getArguments() != null){
                double latitude = getArguments().getDouble(LAT_KEY);
                double longitude = getArguments().getDouble(LNG_KEY);
                update(latitude, longitude);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(SUNRISE_TIME_KEY, sunriseTime);
        outState.putString(SUNRISE_AZIMUTH_KEY, sunriseAzimuth);
        outState.putString(SUNSET_TIME_KEY, sunsetTime);
        outState.putString(SUNSET_AZIMUTH_KEY, sunsetAzimuth);
        outState.putString(TW_MRN_TIME_KEY, twilightMorningTime);
        outState.putString(TW_EV_TIME_KEY, twilightEveningTime);
    }

    private void setView() {
        ((TextView)getView().findViewById(R.id.sunSunriseTime)).setText(sunriseTime);
        ((TextView)getView().findViewById(R.id.sunSunriseAzimuth)).setText(sunriseAzimuth);
        ((TextView)getView().findViewById(R.id.sunSunsetTime)).setText(sunsetTime);
        ((TextView)getView().findViewById(R.id.sunSunsetAzimuth)).setText(sunsetAzimuth);
        ((TextView)getView().findViewById(R.id.sunTwilightMorningTime)).setText(twilightMorningTime);
        ((TextView)getView().findViewById(R.id.sunTwilightEveningTime)).setText(twilightEveningTime);
    }

    private void setValues(double lat, double lng) {
        AstroCalculator.SunInfo sunInfo = AstroCalculatorService.getCurrentSunInfo(lat, lng);

        sunriseTime = String.format(Locale.UK, TIME_FORMAT, sunInfo.getSunrise().getHour(), sunInfo.getSunrise().getMinute());
        sunriseAzimuth = String.format(Locale.UK, COORD_FORMAT, sunInfo.getAzimuthRise());
        sunsetTime = String.format(Locale.UK, TIME_FORMAT, sunInfo.getSunset().getHour(), sunInfo.getSunset().getMinute());
        sunsetAzimuth = String.format(Locale.UK, COORD_FORMAT, sunInfo.getAzimuthSet());
        twilightMorningTime = String.format(Locale.UK, TIME_FORMAT, sunInfo.getTwilightMorning().getHour(), sunInfo.getTwilightMorning().getMinute());
        twilightEveningTime = String.format(Locale.UK, TIME_FORMAT, sunInfo.getTwilightEvening().getHour(), sunInfo.getTwilightEvening().getMinute());
    }

    private void update(double lat, double lng) {
        setValues(lat, lng);
        setView();
    }

    @Override
    public void update(Channel channel, boolean speedUnitSI, boolean tempUnitSI) {
        if(channel != null) {
            setValues(channel.item.lat, channel.item.lng);
            setView();
        }
    }
}
