package com.pam.astroweatherapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pam.astroweatherapp.MainActivity.*;
import com.pam.astroweatherapp.service.AppService;
import com.pam.astroweatherapp.data.Channel;

public class ExtraFragment extends Fragment implements UpdateListener {
    private final static String
        WIND_KM_FORMAT = "%.2f km/h %.2f\u00B0",
        WIND_M_FORMAT = "%.2f mph %.2f\u00B0",
        HUMIDITY_FORMAT = "%.2f %%",
        VISIBILITY_FORMAT = "%.2f Mm";

    private static final String
        WIND_POWER_KEY = "WIND_POWER",
        WIND_DIR_KEY = "WIND_DIR",
        HUMIDITY_KEY = "HUMIDITY",
        VISIBILITY_KEY = "VISIBILITY",
        SPEED_UNIT_KEY = "SPEED_UNIT";

    private double windPower, windDirection, humidity, visibility;
    private boolean speedUnitSI;

    public static ExtraFragment newInstance(Channel channel, boolean sUnitSI, boolean tUnitSI) {
        ExtraFragment extraFragment = new ExtraFragment();

        if(channel != null) {
            Bundle args = new Bundle();
            args.putDouble(WIND_POWER_KEY, channel.wind.speed);
            args.putDouble(WIND_DIR_KEY, channel.wind.direction);
            args.putDouble(HUMIDITY_KEY, channel.atmosphere.humidity);
            args.putDouble(VISIBILITY_KEY, channel.atmosphere.visibility);
            args.putBoolean(SPEED_UNIT_KEY, sUnitSI);
            extraFragment.setArguments(args);
        }

        return extraFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity parentActivity = (MainActivity)context;
        parentActivity.availableFragments.put("ID_EXTRA", this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_extra, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            windPower = savedInstanceState.getDouble(WIND_POWER_KEY);
            windDirection = savedInstanceState.getDouble(WIND_DIR_KEY);
            humidity = savedInstanceState.getDouble(HUMIDITY_KEY);
            visibility = savedInstanceState.getDouble(VISIBILITY_KEY);
            speedUnitSI = savedInstanceState.getBoolean(SPEED_UNIT_KEY);
            setView();
        }
        else if(getArguments() != null) {
            windPower = getArguments().getDouble(WIND_POWER_KEY);
            windDirection = getArguments().getDouble(WIND_DIR_KEY);
            humidity = getArguments().getDouble(HUMIDITY_KEY);
            visibility = getArguments().getDouble(VISIBILITY_KEY);
            speedUnitSI = getArguments().getBoolean(SPEED_UNIT_KEY);
            setView();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(WIND_POWER_KEY, windPower);
        outState.putDouble(WIND_DIR_KEY, windDirection);
        outState.putDouble(HUMIDITY_KEY, humidity);
        outState.putDouble(VISIBILITY_KEY, visibility);
        outState.putBoolean(SPEED_UNIT_KEY, speedUnitSI);
    }

    private void setView() {
        ((TextView)getView().findViewById(R.id.extraHumidityValue)).setText(String.format(HUMIDITY_FORMAT, humidity));
        ((TextView)getView().findViewById(R.id.extraVisibilityValue)).setText(String.format(VISIBILITY_FORMAT, visibility));

        if(speedUnitSI) {
            ((TextView)getView().findViewById(R.id.extraWind)).setText(String.format(WIND_KM_FORMAT, AppService.milesToKilometers(windPower), windDirection));
        }
        else {
            ((TextView)getView().findViewById(R.id.extraWind)).setText(String.format(WIND_M_FORMAT, windPower, windDirection));
        }
    }

    @Override
    public void update(Channel channel, boolean speedUnitSI, boolean tempUnitSI) {
        if(channel != null) {
            this.windPower = channel.wind.speed;
            this.windDirection = channel.wind.direction;
            this.humidity = channel.atmosphere.humidity;
            this.visibility = channel.atmosphere.visibility;
            this.speedUnitSI = speedUnitSI;
            setView();
        }
    }
}
