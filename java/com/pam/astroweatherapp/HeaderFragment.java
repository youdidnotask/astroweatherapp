package com.pam.astroweatherapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class HeaderFragment extends Fragment {
    private final static String CLOCK_FORMAT = "%02d:%02d:%02d";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_header, container, false);
    }

    public void updateClock() {
        GregorianCalendar calendar = new GregorianCalendar(); //TimeZone.getTimeZone(MainActivity.TIMEZONE)
        ((TextView)getView().findViewById(R.id.headerClock)).setText(String.format(
                Locale.UK,
                CLOCK_FORMAT,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND)
        ));
    }
}