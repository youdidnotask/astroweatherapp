package com.pam.astroweatherapp;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astrocalculator.AstroCalculator;
import com.astrocalculator.AstroDateTime;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import com.pam.astroweatherapp.MainActivity.UpdateListener;
import com.pam.astroweatherapp.data.Channel;
import com.pam.astroweatherapp.service.AstroCalculatorService;

public class MoonFragment extends Fragment implements UpdateListener {
    private final static String
            TIME_FORMAT = "%02d:%02d",
            DATE_FORMAT = "%02d.%02d",
            DAY_FORMAT = "%.0f",
            PERCENT_FORMAT = "%.2f%%";

    private final static String
            LAT_KEY = "latitude",
            LNG_KEY = "longitude";

    private final static String
            MOONRISE_TIME_KEY = "MOONRISE_TIME",
            MOONSET_TIME_KEY = "MOONSET_TIME",
            NEW_DATE_KEY = "NEW_DATE",
            FULL_DATE_KEY = "FULL_DATE",
            AGE_DAY_KEY = "AGE_DAY",
            ILLUMINATION_PERCENT_KEY = "ILLUMINATION_PERCENT";

    private String
            moonriseTime, moonsetTime,
            newDate, fullDate,
            ageDay, illuminationPercent;

    public static MoonFragment newInstance(Channel channel, boolean speedUnitSI, boolean tempUnitSI) {
        MoonFragment moonFragment = new MoonFragment();

        if(channel != null) {
            Bundle args = new Bundle();
            args.putDouble(LAT_KEY, channel.item.lat);
            args.putDouble(LNG_KEY, channel.item.lng);
            moonFragment.setArguments(args);
        }

        return moonFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity parentActivity = (MainActivity)context;
        parentActivity.availableFragments.put("ID_MOON", this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_moon, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            moonriseTime = savedInstanceState.getString(MOONRISE_TIME_KEY);
            moonsetTime = savedInstanceState.getString(MOONSET_TIME_KEY);
            newDate = savedInstanceState.getString(NEW_DATE_KEY);
            fullDate = savedInstanceState.getString(FULL_DATE_KEY);
            ageDay = savedInstanceState.getString(AGE_DAY_KEY);
            illuminationPercent = savedInstanceState.getString(ILLUMINATION_PERCENT_KEY);
            setView();
        }
        else {
            if(getArguments() != null){
                double latitude = getArguments().getDouble(LAT_KEY);
                double longitude = getArguments().getDouble(LNG_KEY);
                update(latitude, longitude);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(MOONRISE_TIME_KEY, moonriseTime);
        outState.putString(MOONSET_TIME_KEY, moonsetTime);
        outState.putString(NEW_DATE_KEY, newDate);
        outState.putString(FULL_DATE_KEY, fullDate);
        outState.putString(AGE_DAY_KEY, ageDay);
        outState.putString(ILLUMINATION_PERCENT_KEY, illuminationPercent);
    }

    private void setView() {
        ((TextView)getView().findViewById(R.id.moonMoonriseTime)).setText(moonriseTime);
        ((TextView)getView().findViewById(R.id.moonMoonsetTime)).setText(moonsetTime);
        ((TextView)getView().findViewById(R.id.moonNewMoonDate)).setText(newDate);
        ((TextView)getView().findViewById(R.id.moonFullMoonDate)).setText(fullDate);
        ((TextView)getView().findViewById(R.id.moonAgeDay)).setText(ageDay);
        ((TextView)getView().findViewById(R.id.moonIlluminationPercent)).setText(illuminationPercent);
    }

    private void setValues(double lat, double lng) {
        AstroCalculator.MoonInfo moonInfo = AstroCalculatorService.getCurrentMoonInfo(lat, lng);

        moonriseTime = String.format(Locale.UK, TIME_FORMAT, moonInfo.getMoonrise().getHour(), moonInfo.getMoonrise().getMinute());
        moonsetTime = String.format(Locale.UK, TIME_FORMAT, moonInfo.getMoonset().getHour(), moonInfo.getMoonset().getMinute());
        newDate = String.format(Locale.UK, DATE_FORMAT, moonInfo.getNextNewMoon().getDay(), moonInfo.getNextNewMoon().getMonth());
        fullDate = String.format(Locale.UK, DATE_FORMAT, moonInfo.getNextFullMoon().getDay(), moonInfo.getNextFullMoon().getMonth());
        ageDay = String.format(Locale.UK, DAY_FORMAT, moonInfo.getAge());
        illuminationPercent = String.format(Locale.UK, PERCENT_FORMAT, moonInfo.getIllumination() * 100);
    }

    private void update(double lat, double lng) {
        setValues(lat, lng);
        setView();
    }

    @Override
    public void update(Channel channel, boolean speedUnitSI, boolean tempUnitSI) {
        if(channel != null) {
            setValues(channel.item.lat, channel.item.lng);
            setView();
        }
    }
}
