package com.pam.astroweatherapp;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CityFragment extends Fragment {
    private final static String COORDS_FORMAT = "%.2f\u00B0 %.2f\u00B0";

    private static final String ID_KEY = "id",
        TITLE_KEY = "title",
        LAT_KEY = "lat",
        LNG_KEY = "lng";

    private long id;
    private String title;
    private double lat;
    private double lng;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_city, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(getArguments() != null) {
            id = getArguments().getLong(ID_KEY);
            title = getArguments().getString(TITLE_KEY);
            lat = getArguments().getDouble(LAT_KEY);
            lng = getArguments().getDouble(LNG_KEY);
        }

        set();
    }

    public static CityFragment newInstance(long id, String title, double lat, double lng) {
        CityFragment cityFragment = new CityFragment();

        Bundle args = new Bundle();
        args.putLong(ID_KEY, id);
        args.putString(TITLE_KEY, title);
        args.putDouble(LAT_KEY, lat);
        args.putDouble(LNG_KEY, lng);

        cityFragment.setArguments(args);

        return cityFragment;
    }

    private void set() {
        ((TextView)getView().findViewById(R.id.cityTitle)).setText(title);
        ((TextView)getView().findViewById(R.id.cityCoords)).setText(String.format(COORDS_FORMAT, lat, lng));
    }
}
