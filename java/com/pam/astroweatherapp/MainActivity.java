package com.pam.astroweatherapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.pam.astroweatherapp.data.Channel;
import com.pam.astroweatherapp.data.Data;
import com.pam.astroweatherapp.service.NetworkService;
import com.pam.astroweatherapp.service.SerializationService;
import com.pam.astroweatherapp.service.YahooWeatherService;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity { //FragmentActivity
    private static final int NUM_PAGES = 5;

    public static final String
            WOEID_KEY = "woeid",
            SPEED_KEY = "speed",
            TEMP_KEY = "temp",
            FREQ_KEY = "frequency";

    public static final int WOEID_DEFAULT = 0;
    public static final boolean UNIT_DEFAULT = true;
    public static final int FREQUENCY_DEFAULT = 5;

    private int woeid, woeidBackup;
    protected boolean speedUnitSI = UNIT_DEFAULT;
    protected boolean tempUnitSI = UNIT_DEFAULT;
    private int frequency = FREQUENCY_DEFAULT;

    private HeaderFragment headerFragment;
    private ViewPager viewPager;
    private FragmentPagerAdapter fragmentPagerAdapter;
    private Timer clockTimer, appTimer;
    private TimerTask clockTimerTask, appTimerTask;
    protected HashMap<String, UpdateListener> availableFragments = new HashMap<>();

    private Data channels;
    SerializationService serializationService;
    private String dataFilename, selectedFilename;

    private enum Request {
        SETTINGS,
        LOCATION
    }

    public interface UpdateListener {
        void update(Channel channel, boolean speedUnitSI, boolean tempUnitSI);
    }

    private void init() {
        dataFilename = this.getFilesDir() + "astrodata.ser";
        selectedFilename = this.getFilesDir() + "woeiddata.ser";
        serializationService = new SerializationService(selectedFilename);

        woeid = serializationService.loadSelected();
        channels = new Data(dataFilename);

        Log.e("ASIA", "woeid: " + woeid);
        Log.e("ASIA", "city: " + channels.get(woeid));

        headerFragment = (HeaderFragment)getSupportFragmentManager().findFragmentById(R.id.header);
        viewPager = (ViewPager) findViewById(R.id.pager);
        if(viewPager != null) {
            fragmentPagerAdapter = new betterAdapter(getSupportFragmentManager());
            viewPager.setAdapter(fragmentPagerAdapter);
            viewPager.setOffscreenPageLimit(NUM_PAGES);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null) {
            woeid = savedInstanceState.getInt(WOEID_KEY);
            speedUnitSI = savedInstanceState.getBoolean(SPEED_KEY);
            tempUnitSI = savedInstanceState.getBoolean(TEMP_KEY);
            frequency = savedInstanceState.getInt(FREQ_KEY);
        }
        if(!verifyConnection()) {
            Toast.makeText(getApplicationContext(), "No connection.\nData might be out of date.", Toast.LENGTH_LONG).show();
        }
        init();
        checkLastUpdate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setClockTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        clockTimer.cancel();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(WOEID_KEY, woeid);
        outState.putBoolean(SPEED_KEY, speedUnitSI);
        outState.putBoolean(TEMP_KEY, tempUnitSI);
        outState.putInt(FREQ_KEY, frequency);
    }

    @Override
    protected void onStop() {
        super.onStop();
        serializationService.saveSelected(woeid);
    }

    public class betterAdapter extends FragmentPagerAdapter {
        public betterAdapter(FragmentManager fm) {
            super(fm);
        }

        //returns total number of pages
        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        //returns the appropriate fragment
        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return BaseFragment.newInstance(channels.get(woeid), speedUnitSI, tempUnitSI);
                case 1:
                    return ExtraFragment.newInstance(channels.get(woeid), speedUnitSI, tempUnitSI);
                case 2:
                    return ForecastFragment.newInstance(channels.get(woeid), speedUnitSI, tempUnitSI);
                case 3:
                    return SunFragment.newInstance(channels.get(woeid), speedUnitSI, tempUnitSI);
                case 4:
                    return MoonFragment.newInstance(channels.get(woeid), speedUnitSI, tempUnitSI);
                default:
                    return null;
            }
        }
    }

    public void displaySettingsView(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra(SPEED_KEY, speedUnitSI);
        intent.putExtra(TEMP_KEY, tempUnitSI);
        intent.putExtra(FREQ_KEY, frequency);
        startActivityForResult(intent, Request.SETTINGS.ordinal());
    }

    public void displayLocationsView(View view) {
        Intent intent = new Intent(this, LocationsActivity.class);
        startActivityForResult(intent, Request.LOCATION.ordinal());
    }

    public void displayRefreshed(View view) {
        if(verifyConnection()){
            setAppTimer(0);
        }
        else {
            Toast.makeText(getApplicationContext(), "No connection.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == Request.SETTINGS.ordinal() && resultCode == Activity.RESULT_OK) {
            boolean tmpSpeedUnitSI = data.getBooleanExtra(SPEED_KEY, UNIT_DEFAULT);
            boolean tmpTempUnitSI = data.getBooleanExtra(TEMP_KEY, UNIT_DEFAULT);
            int tmpFreq = data.getIntExtra(FREQ_KEY, FREQUENCY_DEFAULT);

            if(tmpFreq != frequency) {
                speedUnitSI = tmpSpeedUnitSI;
                tempUnitSI = tmpTempUnitSI;
                frequency = tmpFreq;
                long now = Calendar.getInstance().getTimeInMillis();
                long diff = now - channels.get(woeid).lastUpdate;
                long delay = (diff>tmpFreq*60*1000) ? 0 : (tmpFreq*60*1000 - diff);
                setAppTimer(delay);
            }
            else if(tmpSpeedUnitSI != speedUnitSI || tmpTempUnitSI != tempUnitSI) {
                speedUnitSI = tmpSpeedUnitSI;
                tempUnitSI = tmpTempUnitSI;
                refresh();
            }
        }
        if(requestCode == Request.LOCATION.ordinal() && resultCode == Activity.RESULT_OK) {
            int tmpWoeid = data.getIntExtra(WOEID_KEY, WOEID_DEFAULT);

            if(tmpWoeid != woeid) {
                woeidBackup = woeid;
                woeid = tmpWoeid;
                long now = Calendar.getInstance().getTimeInMillis();
                if(!channels.contains(woeid) || (now - channels.get(woeid).lastUpdate) > frequency*60*1000) {
                    if(verifyConnection()) {
                        setAppTimer(0);
                    }
                    else {
                        if(channels.contains(woeid)) {
                            Toast.makeText(getApplicationContext(), "No connection.\nData might be out of date.", Toast.LENGTH_LONG).show();
                            refresh();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "No connection.\nCannot load data.", Toast.LENGTH_LONG).show();
                            woeid = woeidBackup;
                        }
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), "#file loaded", Toast.LENGTH_SHORT).show();
                    refresh();
                }
            }
        }
    }

    private void setClockTimer() {
        clockTimer = new Timer();
        clockTimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        headerFragment.updateClock();
                    }
                });
            }
        };
        clockTimer.scheduleAtFixedRate(clockTimerTask, 0, 1000);
    }

    private void refresh() {
        Toast.makeText(getApplicationContext(), "#refresh", Toast.LENGTH_SHORT).show();
        for (UpdateListener listener : availableFragments.values()) {
            if(listener != null) {
                listener.update(channels.get(woeid), speedUnitSI, tempUnitSI);
            }
        }
    }

    private void checkLastUpdate() {
        long delay=0;
        if(channels.contains(woeid)) {
            long now = Calendar.getInstance().getTimeInMillis();
            long diff = now - channels.get(woeid).lastUpdate;
            delay = (diff > (frequency * 60 * 1000)) ? 0 : (frequency * 60 * 1000 - diff);
        }
        setAppTimer(delay);
    }

    private void setAppTimer(long delay) {
        if(appTimer != null) appTimer.cancel();
        Toast.makeText(getApplicationContext(), "#app timer set", Toast.LENGTH_SHORT).show();
        appTimer = new Timer();
        appTimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(verifyConnection()) {
                            new NetworkLoader().execute(woeid);
                        }
                    }
                });
            }
        };
        appTimer.scheduleAtFixedRate(appTimerTask, delay, frequency*60*1000);
    }

    private boolean verifyConnection() {
        return NetworkService.verifyConnection(this);
    }

    private class NetworkLoader extends AsyncTask<Integer, Void, Channel> {

        @Override
        protected Channel doInBackground(Integer... ids) {
            return YahooWeatherService.getWeatherInfo(ids[0]);
        }

        @Override
        protected void onPostExecute(Channel channel) {
            if(channel != null) {
                Toast.makeText(getApplicationContext(), "#network loaded", Toast.LENGTH_SHORT).show();
                channels.add(woeid, channel);
                refresh();
            }
            else {
                woeid = woeidBackup;
                Toast.makeText(getApplicationContext(), "lack of data!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
