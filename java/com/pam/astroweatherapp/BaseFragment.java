package com.pam.astroweatherapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pam.astroweatherapp.MainActivity.*;
import com.pam.astroweatherapp.service.AppService;
import com.pam.astroweatherapp.data.Channel;

public class BaseFragment extends Fragment implements UpdateListener {
    private final static String
        TEMP_C_FORMAT = "%.0f\u00B0C",
        TEMP_F_FORMAT = "%.0f F",
        COORDS_FORMAT = "%.2f\u00B0 %.2f\u00B0",
        PRESS_FORMAT = "%.2f hPa";

    private final static String
        CODE_KEY = "CODE",
        WEATHER_KEY = "WEATHER",
        CITY_KEY = "CITY",
        LAT_KEY = "LAT",
        LNG_KEY = "LNG",
        TEMP_KEY = "TEMP",
        PRESS_KEY = "PRESS",
        TEMP_UNIT_KEY = "TEMP_UNIT";

    private int code;
    private String weather, city;
    private double lat, lng, temp, pressure;
    private boolean tempUnitSI;

    public static BaseFragment newInstance(Channel channel, boolean sUnitSI, boolean tUnitSI) {
        BaseFragment baseFragment = new BaseFragment();

        if(channel != null) {
            Bundle args = new Bundle();
            args.putInt(CODE_KEY, channel.item.forecast[0].code);
            args.putString(WEATHER_KEY, channel.item.forecast[0].text);
            args.putString(CITY_KEY, channel.location.city);
            args.putDouble(LAT_KEY, channel.item.lat);
            args.putDouble(LNG_KEY, channel.item.lng);
            args.putDouble(TEMP_KEY, channel.item.forecast[0].high);
            args.putDouble(PRESS_KEY, channel.atmosphere.pressure);
            args.putBoolean(TEMP_UNIT_KEY, tUnitSI);
            baseFragment.setArguments(args);
        }

        return baseFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MainActivity parentActivity = (MainActivity)context;
        parentActivity.availableFragments.put("ID_BASE", this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_base, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            code = savedInstanceState.getInt(CODE_KEY);
            weather = savedInstanceState.getString(WEATHER_KEY);
            city = savedInstanceState.getString(CITY_KEY);
            lat = savedInstanceState.getDouble(LAT_KEY);
            lng = savedInstanceState.getDouble(LNG_KEY);
            temp = savedInstanceState.getDouble(TEMP_KEY);
            pressure = savedInstanceState.getDouble(PRESS_KEY);
            tempUnitSI = savedInstanceState.getBoolean(TEMP_UNIT_KEY);
            setView();
        }
        else if(getArguments() != null) {
            code = getArguments().getInt(CODE_KEY);
            weather = getArguments().getString(WEATHER_KEY);
            city = getArguments().getString(CITY_KEY);
            lat = getArguments().getDouble(LAT_KEY);
            lng = getArguments().getDouble(LNG_KEY);
            temp = getArguments().getDouble(TEMP_KEY);
            pressure = getArguments().getDouble(PRESS_KEY);
            tempUnitSI = getArguments().getBoolean(TEMP_UNIT_KEY);
            setView();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CODE_KEY, code);
        outState.putString(WEATHER_KEY, weather);
        outState.putString(CITY_KEY, city);
        outState.putDouble(LAT_KEY, lat);
        outState.putDouble(LNG_KEY, lng);
        outState.putDouble(TEMP_KEY, temp);
        outState.putDouble(PRESS_KEY, pressure);
        outState.putBoolean(TEMP_UNIT_KEY, tempUnitSI);
    }

    private int getResourceId(int code) {
        return this.getResources().getIdentifier("forecast"+code, "drawable", getActivity().getPackageName());
    }

    private void setView() {
        ((TextView)getView().findViewById(R.id.baseWeather)).setText(weather);
        ((TextView)getView().findViewById(R.id.baseCity)).setText(city);
        ((TextView)getView().findViewById(R.id.baseCoords)).setText(String.format(COORDS_FORMAT, lat, lng));
        ((TextView)getView().findViewById(R.id.basePressValue)).setText(String.format(PRESS_FORMAT, pressure));
        ((ImageView)getView().findViewById(R.id.weatherImageView)).setImageResource(getResourceId(code));

        if(tempUnitSI) {
            ((TextView)getView().findViewById(R.id.baseTempValue)).setText(String.format(TEMP_C_FORMAT, AppService.fahrenheitToCelsius(temp)));
        }
        else {
            ((TextView)getView().findViewById(R.id.baseTempValue)).setText(String.format(TEMP_F_FORMAT, temp));
        }
    }

    @Override
    public void update(Channel channel, boolean speedUnitSI, boolean tempUnitSI) {
        if(channel != null) {
            this.code = channel.item.forecast[0].code;
            this.weather = channel.item.forecast[0].text;
            this.city = channel.location.city;
            this.lat = channel.item.lat;
            this.lng = channel.item.lng;
            this.temp = channel.item.forecast[0].high;
            this.pressure = channel.atmosphere.pressure;
            this.tempUnitSI = tempUnitSI;
            setView();
        }
    }
}
